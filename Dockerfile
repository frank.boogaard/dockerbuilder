FROM almalinux:latest
RUN yum install -y java-1.8.0-openjdk-devel wget
ARG nexusurl=10.0.1.5:8081
ARG type=releases
RUN useradd -m -U -d /home/tomcat -s /bin/false tomcat
ADD apache-tomcat.tar.xz /home/tomcat
COPY tomcat-users.xml /home/tomcat/conf/tomcat-users.xml
#COPY filebeat-8.1.2-x86_64.rpm /home/tomcat/filebeat/filebeat-8.1.2-x86_64.rpm
#RUN rpm -vi /home/tomcat/filebeat/filebeat-8.1.2-x86_64.rpm
#COPY filebeat.yml /etc/filebeat/filebeat.yml
RUN wget -c "http://$nexusurl/service/rest/v1/search/assets/download?repository=maven-$type&group=org.springframework.samples&name=spring-framework-petclinic&sort=version&direction=desc&maven.extension=war"  -O /home/tomcat/webapps/paardendokter.war
RUN chmod 755 /home/tomcat/webapps/paardendokter.war
RUN sh -c 'chmod 755 /home/tomcat/*' &&\
chown -R tomcat:tomcat /home/tomcat/*
EXPOSE 8080
CMD tail -f /dev/null
